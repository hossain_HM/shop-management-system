package com.company;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class orders {
    private static float total=0;
    protected Statement s= null;
    protected Connection c=new database().connect();
    protected ResultSet result=null;
    protected int resultUpdate;
    protected String sql;
    private manage acc;
    protected shop shopItem;
    public int makeOrder(int user_id, String time){

            this.sql="INSERT INTO orders ( time, user_id ) VALUES ('"+time+"', '"+user_id+"')";

            try {
                this.s = this.c.createStatement();
                this.resultUpdate = this.s.executeUpdate(this.sql);
            } catch (SQLException e ) {
            }
            if ( this.resultUpdate== 1) {
                this.sql="select max(id) as id from orders";
                try {
                    this.result = this.s.executeQuery(this.sql);
                    try {
                        while (this.result.next() ) {
                            return this.result.getInt("id");
                        }

                    }catch (SQLException e){}
                    System.out.println("inner");

                } catch (SQLException e) {
                    System.out.println("Two");
                }
            } else if(this.resultUpdate==0)
                return 0;

    return 0;
    }
    public boolean makeOrderDetail(int user_id,String time,int order_id, int code, int  num,int type){
        shopItem=new shop();
        ArrayList<String>  sales;
        sales=shopItem.itemDetails(code,type);

        this.sql="INSERT into order_details ( order_id, sales_id,number,price) VALUES ('"+order_id+"'," +
                "'"+Integer.parseInt(sales.get(0))+"','"+num+"','"+Float.parseFloat(sales.get(1))+"')";


        try {
            this.s = this.c.createStatement();
            this.resultUpdate = this.s.executeUpdate(this.sql);
        } catch (SQLException e ) {
        }
        if ( this.resultUpdate== 1) {
            this.printOrder(user_id,time,Integer.parseInt(sales.get(0)),num,Float.parseFloat(sales.get(1)));
            shopItem.updateShop(Integer.parseInt(sales.get(3))-(num*type),Integer.parseInt(sales.get(2)));
            return true;
        } else if(this.resultUpdate==0)
            return false;

        return true;
    }
    private void printOrder (int user_id, String time,int  sales,int  num,float price){
            total +=(price*num);
            System.out.println("A new order");
            System.out.print((user_id) +" "+user_id+" "+sales+" "+num+" at  "+time);
        }

    public float getOrderFinish(float dicount){
        this.acc = new manage();
        this.acc.updateCol("sales",this.total-dicount);
        float value =this.total-dicount;
        this.setPrice();
        return value;
    }

    public void setPrice(){
        total=0;
    }
    //there are a problem in order shape

    public ArrayList<String> myOrders(String date1,String date2,int code1,int code2){
        ArrayList<String> items = new ArrayList<>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="SELECT order_details.number,products.code,products.type ,order_details.price,order_details.price*order_details.number as total FROM `order_details` INNER JOIN orders ON order_details.order_id =orders.id INNER JOIN shop on order_details.sales_id = shop.id INNer JOIn products on shop.product_id = products.id WHERE DATE(orders.time) BETWEEN '"+date1+"' and '"+date2+"' And products.code BETWEEN '"+code1+"' AND '"+code2+"'";
        if(code1==0 ||code2==0)
            this.sql="SELECT order_details.number,products.code,products.type ,order_details.price,order_details.price*order_details.number as total FROM `order_details` INNER JOIN orders ON order_details.order_id =orders.id INNER JOIN shop on order_details.sales_id = shop.id INNer JOIn products on shop.product_id = products.id WHERE DATE(orders.time) BETWEEN '"+date1+"' and '"+date2+"'";
        else if(date1.equals("")||date2.equals(""))
            this.sql="SELECT order_details.number,products.code,products.type ,order_details.price,order_details.price*order_details.number as total FROM `order_details` INNER JOIN orders ON order_details.order_id =orders.id INNER JOIN shop on order_details.sales_id = shop.id INNer JOIn products on shop.product_id = products.id WHERE  products.code BETWEEN '"+code1+"' AND '"+code2+"'";

        try {
            this.result = this.s.executeQuery(this.sql);
            try {

                System.out.println(this.sql);
                while (this.result.next() ) {
                    items.add(this.result.getString("number"));
                    items.add(this.result.getString("code"));
                    items.add(this.result.getString("type"));
                    items.add(this.result.getString("price"));
                    items.add(this.result.getString("total"));
                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return items;
    }
    public float  total(String date1,String date2,int code1,int code2){
        ArrayList<String> items = new ArrayList<>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="SELECT sum(order_details.price*order_details.number) as total FROM `order_details` INNER JOIN orders ON order_details.order_id =orders.id INNER JOIN shop on order_details.sales_id = shop.id INNer JOIn products on shop.product_id = products.id WHERE DATE(orders.time) BETWEEN '"+date1+"' and '"+date2+"' And products.code BETWEEN '"+code1+"' AND '"+code2+"'";
        if(code1==0 ||code2==0)
            this.sql="SELECT sum(order_details.price*order_details.number) as total FROM `order_details` INNER JOIN orders ON order_details.order_id =orders.id INNER JOIN shop on order_details.sales_id = shop.id INNer JOIn products on shop.product_id = products.id WHERE DATE(orders.time) BETWEEN '"+date1+"' and '"+date2+"'";
        else if(date1.equals("")||date2.equals(""))
            this.sql="SELECT sum(order_details.price*order_details.number) as total FROM `order_details` INNER JOIN orders ON order_details.order_id =orders.id INNER JOIN shop on order_details.sales_id = shop.id INNer JOIn products on shop.product_id = products.id WHERE  products.code BETWEEN '"+code1+"' AND '"+code2+"'";

        try {
            this.result = this.s.executeQuery(this.sql);
            try {

                System.out.println(this.sql);
                if (this.result.next() ) {
                    return this.result.getFloat("total");

                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return 0;
    }
   
}

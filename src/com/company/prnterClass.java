package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.util.ArrayList;

public class prnterClass {
    private Object rows[][];
    private Object headers[];
    public void print(ArrayList<String>data,int cols,String dataType) {
        int size = data.size() / cols;
        String[][] multi = new String[size][cols];
        for (int i = 0; i < size; i++) {
            for (int x = 0; x < cols; x++)
                multi[i][x] = data.get((i * cols) + x);
        }
        rows = multi;

        JFrame frame=null;

        if (dataType.equals("products")) {
            headers = new Object[]{"العدد", "التاريخ                         ", "الكود", "الصنف", "السعر", "كود العميل","الاجمالي"};
            frame= new JFrame("احصائيات المشتريات");
        } else if (dataType.equals("acc")) {
            headers = new Object[]{"رصيد نقديه اول مده                           ", "مشتريات", "مبيعات", "مصروفات", "رصيد","تاريخ      "};
            frame= new JFrame("احصائيات المبالغ");
        }else if(dataType.equals("spends")){
            headers=new Object[] {"التاريخ", "الاسم","البند","القيمه"};
            frame = new JFrame("المصروفات");
            }else if(dataType.equals("shop")){
            headers=new Object[] {"العدد","الكود","الصنف","سعر الشراء","اجمالي سعر الشراء","سعر البيع","اجمالي سعر البيع"};
        frame= new JFrame("البضائع المتبقيه");
        }
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        final JTable table = new JTable(rows, headers);
        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        JButton button = new JButton("Print");
        ActionListener printAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    table.print();
                } catch (PrinterException pe) {
                    System.err.println("Error printing: " + pe.getMessage());
                }
            }
        };
        button.addActionListener(printAction);
        frame.add(button, BorderLayout.SOUTH);
        frame.setSize(500, 500);
        frame.setVisible(true);
    }

}

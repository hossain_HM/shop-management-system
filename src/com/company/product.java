package com.company;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class product {
    private int code;

    private  String type;
    private  float price;
    private int client_id;
    private int number;
    private int product_id;
    private manage acc;

    protected Statement s= null;
    protected Connection c=new database().connect();
    protected   ResultSet result=null;
    protected int resultUpdate;
    protected String sql;

    public boolean addProduct(String type,float price,int code,int number,int client_id,float dicount){
            this.number=number;
            this.price=price;
            this.type=type;
            this.client_id=client_id;
            this.code=code;
            this.acc = new manage();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            java.util.Date date = new Date();
            this.product_id=0;
            this.product_id();
            this.sql="INSERT into products ( price, number, date, code, total,client_id, type) VALUES ('"+this.price+
                    "','"+this.number+"','"+dateFormat.format(date)+"','"+this.code+"','"+((this.price*this.number)-dicount)+"','"+this.client_id+"','"
                    +this.type+"')";
            try {
                this.s = this.c.createStatement();
                this.resultUpdate = this.s.executeUpdate(this.sql);
            } catch (SQLException e ) {
            }
            if ( this.resultUpdate== 1) {
                this.addToShop();
                this.acc.updateCol("products",this.price*this.number);
                return true;
            } else if(this.resultUpdate==0)
                return false;
            return true;
    }
    public ArrayList<String> clients()    {
        ArrayList<String> numbers = new ArrayList<String>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="select id,name FROM clients ";
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                while (this.result.next() ) {
                  numbers.add(this.result.getString("id")+" "+this.result.getString("name"));
                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return numbers;
    }
    private int product_id(){
        int id=0;
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="select MAX(id) FROM products WHERE code='"+this.code+"'";
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                if (this.result.next()  ) {
                    id=this.result.getInt("MAX(id)");
                }
            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        this.product_id=id;
        return this.product_id;
    }
    public void updateProduct(int code,int num,String date){
        this.code=code;
        this.sql="update  products SET  number = number - '"+num+"' where code='"+code+"' and  DATE(date) like '"+date+"'";

        try {
            this.s = this.c.createStatement();
            this.resultUpdate = this.s.executeUpdate(this.sql);
        } catch (SQLException e ) {
        }
   if(this.resultUpdate==1){
       this.sql="update  shop SET  number = number - '"+num+"' where product_id ='"+this.product_id()+"'";

       try {
           this.s = this.c.createStatement();
           this.resultUpdate = this.s.executeUpdate(this.sql);
       } catch (SQLException e ) {
       }
   }
    }
    private void addToShop(){
        shop shop=new shop();
        int id=this.product_id;
            if(id!=0){
              shop.editItem(this.price,this.number,this.product_id(),id);
            }else {
                id=this.product_id();
                shop.addItem(this.price,this.number,id);
            }

    }
    public ArrayList<String> allProducts(String date1,String date2,int code1 , int code2,boolean all) {
        ArrayList<String> items = new ArrayList<>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="select * from products  WHERE DATE(date) BETWEEN '"+date1+"' and '"+date2+"' And code BETWEEN '"+code1+"' AND '"+code2+"'";
        if(code1==0 ||code2==0)
            this.sql="select * from products  WHERE DATE(date) BETWEEN '"+date1+"' and '"+date2+"'";
        if(date1.equals("")||date2.equals(""))
            this.sql="select * from products  WHERE  code BETWEEN '"+code1+"' AND '"+code2+"'";
        if(all)
            this.sql="select * from products";

        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                while (this.result.next() ) {
                    items.add(this.result.getString("number"));
                    items.add(this.result.getString("date"));
                    items.add(this.result.getString("code"));
                    items.add(this.result.getString("type"));
                    items.add(this.result.getString("price"));
                    items.add(this.result.getString("client_id"));
                    items.add(this.result.getString("total"));
                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return items;
    }

}


package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class shop {
    protected Statement s= null;
    protected Connection c=new database().connect();
    protected ResultSet result=null;
    protected int resultUpdate;
    protected String sql;
    public boolean addItem(float price,int number,int id){
        this.sql="INSERT into shop ( price, number, product_id) VALUES ('"+(price*130)/100+"', '"+number+"','"+id+"')";
        try {
            this.s = this.c.createStatement();
            this.resultUpdate = this.s.executeUpdate(this.sql);
        } catch (SQLException e ) {
        }
        if ( this.resultUpdate== 1) {
            return true;
        } else if(this.resultUpdate==0)
            return false;
        return true;
    }
    public ArrayList<Integer> getItem(int product_id){
        ArrayList<Integer> item = new ArrayList<Integer>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="select id,number FROM shop WHERE product_id='"+product_id+"'";
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                while (this.result.next() ) {
                    item.add(this.result.getInt("id"));
                    item.add(this.result.getInt("number"));
                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return item;
    }
    public  boolean editItem(float price,int number,int product_id,int old_product_id){
        ArrayList<Integer> item = new ArrayList<Integer>();
        item=this.getItem(old_product_id);
        number +=item.get(1);
        this.sql="update  shop SET  price='"+(price*130)/100+"', number='"+number+"', product_id = '"+product_id+"' where product_id='"+old_product_id+"'";
        try {
            this.s = this.c.createStatement();
            this.resultUpdate = this.s.executeUpdate(this.sql);
        } catch (SQLException e ) {
        }
        if ( this.resultUpdate== 1) {
            return true;
        } else if(this.resultUpdate==0)
            return false;
        return true;
    }
    public ArrayList<String> itemDetails(int code,int type){
        ArrayList<String> itemInfo = new ArrayList<>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="select shop.*,products.type FROM shop INNER JOIN products on shop.product_id=products.id WHERE products.code='"+code+"'";
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                while (this.result.next() ) {
                    itemInfo.add(this.result.getString("id"));
                    itemInfo.add(String.valueOf(Float.parseFloat(this.result.getString("price"))*type));
                    itemInfo.add(this.result.getString("product_id"));
                    itemInfo.add(this.result.getString("number"));
                    itemInfo.add(this.result.getString("type"));
                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return itemInfo;
    }
    //used in orders class
    public void updateShop(int num,int product_id){
        this.sql="update  shop SET  number='"+num+"' where product_id='"+product_id+"'";
        try {
            this.s = this.c.createStatement();
            this.resultUpdate = this.s.executeUpdate(this.sql);
        } catch (SQLException e ) {
        }
    }
    public ArrayList<String> allItems( int code1,int code2,boolean all){
        ArrayList<String> items = new ArrayList<>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        if(!all)
        this.sql="select shop.price as shopPrice,shop.number,shop.price*shop.number as totalShopPrice,products.type,products.code,products.price as productPrice, products.price*shop.number as productTotalPrice FROM shop INNER JOIN products on shop.product_id=products.id where products.code BETWEEN '"+code1+"' AND '"+code2+"'";
        else
            this.sql="select shop.price as shopPrice,shop.number,shop.price*shop.number as totalShopPrice,products.type,products.code,products.price as productPrice, products.price*shop.number as productTotalPrices FROM shop INNER JOIN products on shop.product_id=products.id";
        System.out.println(this.sql);
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                while (this.result.next() ) {
                items.add(this.result.getString("number"));
                items.add(this.result.getString("code"));
                items.add(this.result.getString("type"));
                items.add(this.result.getString("shopPrice"));
                items.add(this.result.getString("totalShopPrice"));
                items.add(this.result.getString("productPrice"));
                items.add(this.result.getString("productTotalPrices"));

                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return items;
    }
    public float getDaySales() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = new Date();

        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql = "SELECT SUM(price) FROM `order_details` INNER JOIN orders ON order_details.order_id=orders.id WHERE DATE(orders.time) LIKE '" + dateFormat.format(date) + "'";
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                if (this.result.next()) {
                    return this.result.getFloat("SUM(price)");
                }

            } catch (SQLException e) {
            }
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return 0;
    }
}

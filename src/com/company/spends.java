package com.company;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
public class spends {
    private static int total=0;
    protected Statement s= null;
    protected Connection c=new database().connect();
    protected ResultSet result=null;
    protected int resultUpdate;
    protected String sql;
    protected manage acc;
    protected shop shopItem;
    public boolean addSpend(String category,String child){
        int spendtypes_id;
        if(category=="cars")
           spendtypes_id=1;
        else if(category=="general")
            spendtypes_id=2;
        else
            spendtypes_id=3;
        this.sql="INSERT INTO spends ( name, spendtypes_id ) VALUES ('"+child+"', '"+spendtypes_id+"')";

        try {
            this.s = this.c.createStatement();
            this.resultUpdate = this.s.executeUpdate(this.sql);
        } catch (SQLException e ) {
        }
        if ( this.resultUpdate== 1) {
            return true;
        } else if(this.resultUpdate==0)
            return false;

        return false;
    }
    public void spend(int value,int id){
        this.acc = new manage();
        this.sql="INSERT INTO spends_values ( value, spends_id ) VALUES ('"+value+"', '"+id+"')";

        try {
            this.s = this.c.createStatement();
            this.resultUpdate = this.s.executeUpdate(this.sql);
        } catch (SQLException e ) {
        }
        if ( this.resultUpdate== 1)
            this.acc.updateCol("spends",value);
    }
    public ArrayList<String> allSpends() {
        ArrayList<String> items = new ArrayList<>();
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="select spends.id as spendsid,spends.name as spendsname,spendtypes.id as typesid,spendtypes.name as typesname " +
                "FROM spends INNER JOIN spendtypes on spends.spendtypes_id=spendtypes.id ORDER BY spendtypes.id ASC ";
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                while (this.result.next() ) {
                    items.add(this.result.getString("spendsid"));
                    items.add(this.result.getString("spendsname"));
                    items.add(this.result.getString("typesid"));
                    items.add(this.result.getString("typesname"));
                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return items;
    }
    public ArrayList<String>spendsValues(String date1 ,String date2,String category,boolean all){
        ArrayList<String> items = new ArrayList<>();
        String str = "DATE(spends_values.date) as date ,";
        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="SELECT " +str+" spends.name,spendtypes.name as category,spends_values.value FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id";
        if(!all){
            this.sql="SELECT " +str+" spends.name,spendtypes.name as category,spends_values.value FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id WHERE DATE(spends_values.date) BETWEEN '"+date1+"' and '"+date2+"'  AND spendtypes.name = '"+category+"'";
            if(category.equals(""))
                this.sql="SELECT " +str+" spends.name,spendtypes.name as category,spends_values.value FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id WHERE DATE(spends_values.date) BETWEEN '"+date1+"' and '"+date2+"'";
            if(date1.equals("") ||date2.equals(""))
                this.sql="SELECT " +str+" spends.name,spendtypes.name as category,spends_values.value FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id WHERE  spendtypes.name = '"+category+"'";
        }
        System.out.println(this.sql);
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                while (this.result.next() ) {
                    items.add(this.result.getString("date"));
                    items.add(this.result.getString("name"));
                    items.add(this.result.getString("category"));
                    items.add(this.result.getString("value"));

                }

            }catch (SQLException e){}
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return items;
    }
    public float getDaySpends(String date1 ,String date2,String category,boolean all) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = new Date();

        try {
            this.s = this.c.createStatement();
        } catch (SQLException e) {
            System.out.println("one");
        }
        this.sql="SELECT SUM(spends_values.value) as val FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id";
        if(!all){
            this.sql="SELECT SUM(spends_values.value) as val FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id WHERE DATE(spends_values.date) BETWEEN '"+date1+"' and '"+date2+"'  AND spendtypes.name = '"+category+"'";
            if(category.equals(""))
                this.sql="SELECT SUM(spends_values.value) as val FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id WHERE DATE(spends_values.date) BETWEEN '"+date1+"' and '"+date2+"'";
            if(date1.equals("") ||date2.equals(""))
                this.sql="SELECT SUM(spends_values.value) as val FROM `spends_values` INNER JOIN spends ON spends_values.spends_id =spends.id INNER JOIN spendtypes ON spends.spendtypes_id=spendtypes.id WHERE  spendtypes.name = '"+category+"'";
        }
        System.out.println(this.sql);
        try {
            this.result = this.s.executeQuery(this.sql);
            try {
                if (this.result.next()) {
                    return this.result.getFloat("val");
                }

            } catch (SQLException e) {
            }
            System.out.println("inner");

        } catch (SQLException e) {
            System.out.println("Two");
        }
        return 0;
    }

}
